import React from 'react';
import Auxs from '../../hoc/Auxs';
import  classes from './Layout.css';
const layout = (props) => (
  <Auxs>
    <div>
      <div>Toolbar,SideDrawer,BackDrop</div>
      <main className={classes.Content}>
        {props.children}
      </main>
    </div>
  </Auxs>
);

export default layout;
